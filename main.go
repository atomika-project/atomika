package main

import (
	"embed"
	"fmt"
	"gitlab.com/atomika-project/atomika/commands"
	"os"
)

var Version = "development"

//go:embed all:template/assets
var assets embed.FS

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run() error {

	di := commands.DI{
		AtomikaFS: assets,
		Version:   Version,
	}
	cmd := commands.New(di)

	res := cmd.Execute(os.Args[1:])
	if res.Err != nil {
		return res.Err
	}

	fmt.Println(res.Result)
	return nil
}
