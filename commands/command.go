package commands

import (
	"embed"
	"fmt"
	"github.com/spf13/cobra"
)

var CmdAtomika *cobra.Command

func init() {
	CmdAtomika = &cobra.Command{
		Use: "atomika",
	}
}

type DI struct {
	AtomikaFS embed.FS
	Version   string
}

type Execute func(cmd *cobra.Command, args []string) error

type Cmd interface {
	CobCmd() *cobra.Command
}

type Command struct {
	cmdAtomika *cobra.Command
	atomikaFS  embed.FS
	version    string
}

func (c *Command) Execute(args []string) Response {
	var res Response

	buildCommands([]Cmd{
		newCmdValue(cmdVersionDeps{Version: c.version}),
	})

	fmt.Printf("ver %v \n", c.version)

	c.cmdAtomika.SetArgs(args)
	res.Cmd, res.Err = c.cmdAtomika.ExecuteC()

	return res
}

type Response struct {
	Result string
	Err    error
	Cmd    *cobra.Command
}

func New(di DI) *Command {
	cmd := &Command{
		cmdAtomika: CmdAtomika,
		atomikaFS:  di.AtomikaFS,
		version:    di.Version,
	}
	fmt.Printf("ver new %v \n", di)
	return cmd
}

func buildCommands(commands []Cmd) {
	for _, c := range commands {
		cmd := c.CobCmd()
		if cmd == nil {
			continue
		}
		CmdAtomika.AddCommand(cmd)
	}
}

func createCommand(use, short string, alias []string, runE Execute) *cobra.Command {
	return &cobra.Command{
		Use:     use,
		Short:   short,
		Aliases: alias,
		RunE:    runE,
	}
}
