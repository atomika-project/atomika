package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
)

const (
	CmdVersionUse   = "version"
	CmdVersionShort = "output version"
)

var CmdVersionAlias = []string{"v", "ver"}

type cmdVersionDeps struct {
	Version string
}

type cmdVersion struct {
	cobComm *cobra.Command
	res     cmdVersionDeps
}

func (c *cmdVersion) CobCmd() *cobra.Command {
	return c.cobComm
}

func (c *cmdVersion) runE(_ *cobra.Command, args []string) error {
	fmt.Printf("args :%+v", args)
	log.Println("Ver", c.res.Version)
	return nil
}

func newCmdValue(di cmdVersionDeps) Cmd {
	c := &cmdVersion{
		res: di,
	}

	c.cobComm = createCommand(
		CmdVersionUse,
		CmdVersionShort,
		CmdVersionAlias,
		c.runE,
	)

	return c
}
